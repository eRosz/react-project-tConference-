import React, {Component} from 'react';

import {connect} from 'react-redux';

import {Button} from 'reactstrap';

import {pushConference} from '../../store/actions';

class PostForm extends Component {
    constructor(props) {
        super(props);
    }

    onClick = (e) => {
        e.preventDefault();
        this.props.pushConference(this.refs.location.value, this.refs.details.value);
    }

    render() {
        return (
            <div>
                <h4>Location</h4>
                <input className="formInput" ref="location" style={{width: "60%"}}></input>

                <h4>Conference details</h4>
                <textarea className="formInput" style={{height:"20vh", width:"100%"}} type="textarea" name="text" ref="details"/>
                <div style={{marginTop: "5%", textAlign: "right"}}>
                    <Button onClick={this.onClick} style={{backgroundColor: "#00000066"}}>Reserve</Button>
                </div>
            </div>
        );
    }
}

export default connect(null, {pushConference})(PostForm)
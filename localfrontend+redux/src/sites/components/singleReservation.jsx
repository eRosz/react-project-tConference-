import React from 'react';
import {connect} from 'react-redux';
import {deleteConference} from '../../store/actions'

const SingleReservation = props => {
    return (
        <div style={{
            backgroundColor: "#FFFFFF66",
            borderRadius: "10px"
        }}>
            <div style={{display: "flex"}}>
            <h1 style={{paddingLeft: "5px", width: "100%", marginBottom: "0px"}}>{props.reservation.title}</h1>
            <h1 className="deleteButton" onClick={() => props.deleteReservation(props.reservation.id)}>X</h1>
            </div>
            
            <hr/>
            <p style={{paddingLeft: "5px"}}>{props.reservation.content}</p>
        </div>
    );
};

export default connect(null, { deleteReservation: deleteConference })(SingleReservation);
import React from 'react';
    
import PostDetails from './PostDetails';
import PostForm from './PostForm';

const PostView = () => {
    return (
        <div style={{
            padding: "10%"
        }}>
            
            <div className="reservations" style={{
                backgroundColor: "#FFFFFF66"
            }}>
                <h1>Reserve a conference</h1>
                <hr/>
                <PostDetails />
                <PostForm />
            </div>
        </div>
    );
}

export default PostView;
import React from 'react';

import {connect} from 'react-redux';

const PostDetails = props => {
    return (
        <div>
            <div>
                <p>Here you can reserve a conference to this system, by inserting the location of the conference to the 'Location' input. 
                    Giving more details about your conference might boost its' popularity, which is why you can add more details to the 
                    'Conference details' input. After that, all you need to is press 'Reserve'</p>
            </div>
            <div style={{display: "flex", direction:"rtl"}}>
                <div style={{
                    color: "white", padding: "5px",
                    marginTop: "-30px", marginBottom: "10px",
                    backgroundColor: "#00000066",
                    borderRadius: "5px"
                }}>
                    <p style={{marginBottom: "0px"}}>Amount of reservations: {props.resCount}</p>
                </div>
            </div>
        </div>
    );
}

export default connect((state) => {
    return {
        resCount: state.amountOfReservations
    };
})(PostDetails);
export const types = {
    PUSH_CONFERENCE: "PUSH_CONFERENCE",
    DELETE_CONFERENCE: "DELETE_CONFERENCE"
};

export const pushConference = (location, info) => (dispatch) => {
    dispatch({
        type: types.PUSH_CONFERENCE,
        payload: {
            title: location,
            content: info
        }
    });
}

export const deleteConference = (id) => (dispatch) => {
    dispatch({
        type: types.DELETE_CONFERENCE,
        payload: {
            removable: id
        }
    });
}

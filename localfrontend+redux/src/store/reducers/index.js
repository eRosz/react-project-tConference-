import {types} from '../actions/'

export const initialState = {
    reservations: [],
    amountOfReservations: 0
};

export const mainReducer = (state, action) => {
    let nState = state;
    switch(action.type) {
        case types.PUSH_CONFERENCE:
            const addedAmount = state.reservations.length + 1;
            nState = {
                ...state,
                reservations: [{...action.payload, id: addedAmount}, ...state.reservations],
                amountOfReservations: addedAmount
            }
            break;

        case types.DELETE_CONFERENCE:
            const reducedAmount = state.reservations.length - 1;
            console.log(action.payload);
            nState = {
                ...state,
                reservations: state.reservations.filter((currVal) => {
                    console.log(currVal.id, action.payload.removable);
                    return currVal.id !== action.payload.removable
                }),
                amountOfReservations: reducedAmount
            }
            break;
        default:
            nState = state;

    }

    console.log(action.type, state, nState);
    return nState;
};